<%-- 
    Document   : index
    Created on : 23-Oct-2019, 11:30:03
    Author     : simon
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP-Calc</title>
    </head>
    <body>
        <h1>Simple Calculator</h1>
        <p>Enter three numbers to add:</p>
        <form name="Input Form" action="response.jsp">
            Number 1: <input type="text" name="num1" /><br/>
            Number 2: <input type="text" name="num2" /><br/>
            Number 3: <input type="text" name="num3" /><br/>
            <input type="submit" value="Submit" />
        </form>
    </body>
</html>
